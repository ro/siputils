import setuptools
import os
import glob
if os.path.exists('README.md'):
    with open("README.md", "r") as fh:
        long_description = fh.read()
else:
    long_description = ''

scripts = glob.glob('bin/*')

setuptools.setup(
    name="siputils",
    version="2.8.1",
    setuptools_git_versioning={
        "template": "{tag}",
        "dev_template": "{tag}.dev{ccount}+git.{sha}",
        "dirty_template": "{tag}.dev{ccount}+git.{sha}.dirty",
        "starting_version": "0.0.1",
        "version_file": 'version',
        "count_commits_from_version_file": True
    },
    author="Mattia Mancini",
    author_email="mancini@astron.nl",
    description="LTA sip utils",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.astron.nl/ro/siputils",
    packages=['lofar_lta_sip'],
    package_dir={'lofar_lta_sip': 'lib'},
    package_data = {'lofar_lta_sip': ['etc/*.xsd','etc/.siplibrc','StationPositions.parset']},
    include_package_data=False,
    scripts=scripts,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache2 License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires = [
        'pyxb==1.2.5',
        'lxml',
        'requests',
        'graphviz'
    ],
    tests_require=['pytest'],
    setup_requires = [
        'setuptools-git-versioning<=1.8.1'
    ],
    test_suite='test',
)

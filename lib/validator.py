import pkg_resources
from lxml import etree
import sys
from . import ltasip, siplib
import logging
import os
LOGLEVEL = logging.DEBUG

logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=LOGLEVEL, stream=sys.stderr)
logger = logging.getLogger(__name__)

#DEFAULT_SIP_XSD_PATH = pkg_resources.resource_string(__name__, "etc/LTA-SIP.xsd")
DEFAULT_SIP_XSD_PATH = os.path.join(os.path.dirname(__file__), "etc/LTA-SIP.xsd")


def validate(xmlpath, xsdpath=DEFAULT_SIP_XSD_PATH):
    '''validates given xml file against given xsd file'''

    logger.info("validating %s against %s" % (xmlpath, xsdpath))

    with open(xsdpath) as xsd:
        xmlschema_doc = etree.parse(xsd)
        xmlschema = etree.XMLSchema(xmlschema_doc)

        with open(xmlpath) as xml:
            doc = etree.parse(xml)
            valid = xmlschema.validate(doc)

            if not valid:
                logger.info("SIP is NOT valid according to schema definition!")
                try:
                    xmlschema.assertValid(doc)
                except Exception as err:
                    logger.error(err)
            else:
                logger.info("SIP is VALID according to schema definition!")
            return valid


def check_consistency(sip):
    '''
    since a SIP can be valid XML but make no sense structurally, this makes sure that SIP contents contain a tree
    structure that ends with a single described dataproduct, and all pipelines and intermediary dataproducts are
    properly chained together.
    '''

    sip = sip._get_pyxb_sip(suppress_warning=True)
    linkstodataproduct = {}
    linkstoprocess = {}

    # the dataproduct that is described by the sip
    data_out = sip.dataProduct
    id_out = str(data_out.dataProductIdentifier.identifier)
    id_process = str(data_out.processIdentifier.identifier)
    linkstodataproduct.setdefault(id_out, []).append(id_process)

    # the input / intermediate dataproducts
    for data_in in sip.relatedDataProduct:
        id_in = str(data_in.dataProductIdentifier.identifier)
        id_process = str(data_in.processIdentifier.identifier)
        linkstodataproduct.setdefault(id_in, []).append(id_process)

    # the observations
    for obs in sip.observation:
        id_obs = str(obs.observationId.identifier)
        id_process = str(obs.processIdentifier.identifier)
        linkstoprocess.setdefault(id_process, [])

    # the data processing steps
    for pipe in sip.pipelineRun:
        id_pipe = str(pipe.processIdentifier.identifier)
        id_in = []
        for elem in pipe.sourceData.orderedContent():
            id_in.append(str(elem.value.identifier))
        linkstoprocess.setdefault(id_pipe, []).append(id_in)

    # the data processing steps
    for unspec in sip.unspecifiedProcess:
        id_unspec = str(unspec.processIdentifier.identifier)
        linkstoprocess.setdefault(id_unspec, [])

    # todo: online processing
    # todo: parsets (?)

    for id in linkstodataproduct:
        for id_from in linkstodataproduct.get(id):
            if not id_from in linkstoprocess:
                raise Exception(
                    "The pipeline or observation that created dataproduct '" + id + "' seems to be missing! -> ",
                    id_from)

    for id in linkstoprocess:
        for ids_from in linkstoprocess.get(id):
            for id_from in ids_from:
                if not id_from in linkstodataproduct:
                    raise Exception("The input dataproduct for pipeline '" + id + "' seems to be missing! -> ", id_from)

    logger.info("General SIP structure for dataproduct with sip_id=%s seems ok!", id_out)
    return True  # already raised Exception if there was a problem...


def check_consistency_of_file(xmlpath):
    """
    Checks the general structure of the provided SIP XML. E.g.:
    Is/Are the processes/es present that created the described dataproduct / related dataproducts?
    Are the input dataproducts for these processes present?
    """

    logger.info("Checking %s for structural consistency" % xmlpath)

    with open(xmlpath) as f:
        xml = f.read()
        sip = siplib.Sip.from_xml(xml)
        return check_consistency(sip)


def main(xml, xsd_file=None):
    """
    validates given xml against the SIP XSD and does consistency check
    """

    try:
        xml = xml
        if xsd_file is None:
            xsd = DEFAULT_SIP_XSD_PATH
        else:
            xsd = xsd_file
        valid = validate(xml, xsd)
        consistent = check_consistency_of_file(xml)
        return valid and consistent
    except Exception as err:
        logger.error(err)

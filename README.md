**About**

The SIP library is a Python library to assist in creating a valid LOFAR SIP (Submission Information Package) that is required to put data in the Astron LTA (Long Term Archive). It also contains some utilities to validate and visulaize existing SIPs.

**General Structure**

This library  essentially consists of an auto-generated part ([ltasip.py](lib/ltasip.py)), 
which generates the XML and was itself generated from the XSD schema using pyxb, and a written part ([siplib.py](lib/siplib.py)) that wraps the generated part, 
mainly to provide nicer constructors since the pyxb-generated code is a little hard to work against.

The ltasip.py file can be generated from the LTA SIP schema. Make sure you have installed pyxb (pip install pyxb):
    
    pyxbgen -u <path_to>/LTA-SIP.xsd
Copy/paste the generated binding.py in the ltasip.py but watch out for the path used in pyxb.utils.utility.Location
which should have DEFAULT_SIP_XSD_PATH as first argument and the 'generated path', so find-replace this as well.  

**Getting Started**

This library is somewhat work in progress, so there is not yet a real end user documentation in that sense. 

But there is some inline documentation in [lib/siplib.py](siplib.py) and you may want to check out [test/example.py](example.py) to see how this can be used.


**Versioning**

The initial version of the siblib in this repo is version 0.4

Because the siplib is strongly coupled to the SIP xsd, the version numbering of the siplib will match with the version of the xsd schema which is 2.8.1 now.
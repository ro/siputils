#!/usr/bin/env python3

# Copyright (C) 2012-2015    ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id: $

try:
    import pyxb
except ImportError as e:
    print(str(e))
    print('Please install python3 package pyxb: sudo apt-get install python3-pyxb')
    exit(3)    # special lofar test exit code: skipped test

import unittest
import os
from lofar_lta_sip import siplib, validator, constants, feedback
from unittest.mock import patch

import uuid
import logging
logger = logging.getLogger(__name__)

TMPFILE_PATH = "test_siplib.xml"
working_dir = os.path.dirname(os.path.abspath(__file__))
FEEDBACK_PATH = os.path.join(working_dir, "test_files/testmetadata_file.Correlated.modified")

id_response = {"version": "version",
               "result": "ok",
               "id": uuid.uuid1().int >> 64,
               "user_label": None,
               "data_type": "type",
               "identifier_source": 'test',
               "is_new": True,
               "error": ''}

def create_basicdoc():
    with patch('lofar_lta_sip.siplib.query.client') as client_mock:
        client_mock.GetUniqueID.return_value = id_response
        return siplib.Sip(
                project_code = "code",
                project_primaryinvestigator = "pi",
                project_contactauthor = "coauthor",
                # project_telescope="LOFAR",
                project_description = "awesome project",
                project_coinvestigators = ["sidekick1", "sidekick2"],
                dataproduct = siplib.SimpleDataProduct(
                    siplib.DataProductMap(
                        type = "Unknown",
                        identifier = siplib.Identifier('test'),
                        size = 1024,
                        filename = "/home/paulus/test.h5",
                        fileformat = "HDF5",
                        storage_writer='LofarStorageManager',
                        storage_writer_version = 'storagewriterversion',
                        process_identifier = siplib.Identifier('test'),
                        checksum_md5 = "hash1",
                        checksum_adler32 = "hash2",
                        storageticket = "ticket"
                    )
                )
            )

class TestSIPfeedback(unittest.TestCase):

    def test_basic_doc(self):
        # create example doc with mandatory attributes
        logging.info("===\nCreating basic document:\n")
        mysip = create_basicdoc()
        mysip.save_to_file(TMPFILE_PATH)
        self.assertTrue(validator.validate(TMPFILE_PATH))

    def test_dataproducts(self):

        with patch('lofar_lta_sip.siplib.query.client') as client_mock:
            client_mock.GetUniqueID.return_value = id_response
            mysip = create_basicdoc()
            logging.info("===\nAdding related generic dataproduct:\n")
            with open(FEEDBACK_PATH) as f:
                text = f.readlines()
                fb = feedback.Feedback(text)
            pipe_label = siplib.Identifier('test')
            dataproducts = fb.get_dataproducts(prefix = "test.prefix", process_identifier = pipe_label)
            for dp in dataproducts:
                logging.info("...adding:", dp)
                mysip.add_related_dataproduct(dp)

            mysip.save_to_file(TMPFILE_PATH)
            self.assertTrue(validator.validate(TMPFILE_PATH))

# run tests if main
if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3

# Copyright (C) 2012-2015    ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id: $

try:
    import pyxb
except ImportError as e:
    print(str(e))
    print('Please install python3 package pyxb: sudo apt-get install python3-pyxb')
    exit(3)    # special lofar test exit code: skipped test

import unittest
import os
from lofar_lta_sip import validator

VALIDFILE_PATH = os.path.join(os.path.dirname(__file__), "test_files/valid_sip.xml")


class TestSIPvalidator(unittest.TestCase):
    def test_validate(self):
        self.assertTrue(validator.validate(VALIDFILE_PATH))
        self.assertTrue(validator.check_consistency_of_file(VALIDFILE_PATH))
        self.assertTrue(validator.main(VALIDFILE_PATH))

    def test_validate_of_cobalt2_extensions(self):
        """
        Test with the SIP files which are references of the COBALT2 LTA/SIP update
        See SDC-545 for a complete overview
        - Integrate topocentric frequency correction -> correlated-doppler.sip.xml
        - Integrate spectral leakage, quantization, repointing and multiple beamformer pipelines  -> beamformed.sip.xml
        """
        lst_test_files = ["correlated-doppler.sip.xml", "beamformed.sip.xml"]
        for file in lst_test_files:
            full_file_name = os.path.join(os.path.dirname(__file__), "test_files", file)
            self.assertTrue(validator.validate(full_file_name))
            self.assertTrue(validator.check_consistency_of_file(full_file_name))
            self.assertTrue(validator.main(full_file_name))



# run tests if main
if __name__ == '__main__':
    unittest.main()
